import java.util.Scanner;

public class ArrayManipulation {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int[] numbers = {5,8,3,2,7};
        String[] names = {"Alice", "Bob", "Charlie", "David"};
        double[] values = new double[4];
        int sum = 0;

        for(int i=0;i<numbers.length;i++){
            System.out.print(numbers[i]+" ");
        }

        System.out.println("");

        for(String i : names){
            System.out.print(i+" ");
        }

        System.out.println("");

        for(int i=0;i<values.length;i++){
            values[i] = sc.nextDouble();
        }

        for(int i=0;i<numbers.length;i++){
            sum=sum+numbers[i];
        }
        System.out.print(sum);

        System.out.println("");
        
        double max = 0;
        for(int i=0;i<values.length;i++){
            if(values[i]> max){
                max = values[i];
            }
        }
        System.out.print(max);
    }
}
